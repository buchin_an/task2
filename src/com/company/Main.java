package com.company;

public class Main {

    public static void main(String[] args) {

        int coefficientA = 1;
        int coefficientB = 2;
        int coefficientC = 3;

        double discriminant;

        discriminant = Math.pow(coefficientB, 2) - (4 * coefficientA * coefficientC);
        if (discriminant >= 0) {
            System.out.println("Корни уравнения:");
            System.out.println((-coefficientB - Math.sqrt(discriminant)) / (2 * coefficientA));
            System.out.println((-coefficientB + Math.sqrt(discriminant)) / (2 * coefficientA));
        }
        if (discriminant < 0) {
            System.out.println("Корни уравнения комплексные числа:");
            discriminant = -discriminant;
            System.out.println((-coefficientB - Math.sqrt(discriminant)) / (2 * coefficientA));
            System.out.println((-coefficientB + Math.sqrt(discriminant)) / (2 * coefficientA));
        }
    }
}
